package cz.cvut.fel.swa.bookmaker.bookmakerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookmakerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookmakerServiceApplication.class, args);
	}

}
