package cz.cvut.fel.swa.bookmaker.bookmakerservice.domain;

import lombok.Data;

@Data
public class Odds {
    public Long id;
}
