package cz.cvut.fel.swa.bookmaker.bookmakerservice.client.converter;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.dto.UserDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.User;

public class UserConverter {

    public static User toModel(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        return user;
    }
}
