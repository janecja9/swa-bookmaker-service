package cz.cvut.fel.swa.bookmaker.bookmakerservice.api.controller;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.api.converter.BetConverter;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto.BetDetailDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto.BetDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.business.BetService;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.OddsClient;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.UserClient;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.exception.ServiceUnavailableException;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Bet;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

@RestController
@RequiredArgsConstructor
public class BetController {

    private final UserClient userClient;
    private final OddsClient oddsClient;
    private final BetService betService;

    @GetMapping("/users/{id}/bets")
    @Operation(summary = "Get all bets", description = "Get all bets for the given user if the user exists")
    @ApiResponse(responseCode = "200", description = "List of bets returned")
    @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    @ApiResponse(responseCode = "503", description = "Service unavailable", content = @Content)
    ResponseEntity<Collection<BetDetailDto>> all(@PathVariable long id) {
        try {
            if (userClient.getUser(id).isEmpty()) {
                return ResponseEntity.notFound().build();
            }
            var bets = betService.findAllByUserId(id);
            return ResponseEntity.ok().body(BetConverter.fromModels(bets));
        } catch (ServiceUnavailableException e) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                    .header("Retry-After", String.valueOf(TimeUnit.MINUTES.toSeconds(1)))
                    .build();
        }
    }

    @PostMapping("/users/{id}/bets")
    @Operation(summary = "Place bet", description = "Place bet for the specified user on the given odds if both the user and odds data are valid.")
    @ApiResponse(responseCode = "201", description = "Bet created successfully")
    @ApiResponse(responseCode = "404", description = "User or odds not found/valid", content = @Content)
    @ApiResponse(responseCode = "503", description = "Service unavailable", content = @Content)
    ResponseEntity<?> create(@PathVariable long id, @RequestBody BetDto betDto) {
        try {
            if (id != betDto.getUserId()
                    || userClient.getUser(betDto.getUserId()).isEmpty()
                    || oddsClient.getOdds(betDto.getOddsId()).isEmpty()) {
                return ResponseEntity.notFound().build();
            }
            Bet bet = BetConverter.toModel(betDto);
            betService.create(bet);

            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (ServiceUnavailableException e) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                    .header("Retry-After", String.valueOf(TimeUnit.MINUTES.toSeconds(1)))
                    .build();
        }
    }
}
