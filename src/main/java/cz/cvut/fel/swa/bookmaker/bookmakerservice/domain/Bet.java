package cz.cvut.fel.swa.bookmaker.bookmakerservice.domain;

import lombok.Data;

import jakarta.persistence.*;

@Entity
@Data
public class Bet {
    @Id
    @GeneratedValue
    private Long id;
    private Long userId;
    private Long oddsId;
    @Column(name = "bet_value")
    private Integer value;
}
