package cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto;

import lombok.Data;

@Data
public class UserDto {
    public Long id;
}
