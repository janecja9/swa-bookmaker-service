package cz.cvut.fel.swa.bookmaker.bookmakerservice.client;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.converter.OddsConverter;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.dto.OddsDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.exception.ServiceUnavailableException;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Odds;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class OddsClient {

    private final LoadBalancerClient loadBalancerClient;
    private final WebClient.Builder webClientBuilder;

    public Optional<Odds> getOdds(Long id) throws ServiceUnavailableException {
        ServiceInstance instance = loadBalancerClient.choose("ODDS-SERVICE");
        if (instance == null) {
            throw new ServiceUnavailableException();
        }

        String baseUrl = instance.getUri().toString();
        try {
            var oddsDto = webClientBuilder
                    .baseUrl(baseUrl)
                    .build()
                    .get()
                    .uri("/odds/{id}", id)
                    .retrieve()
                    .bodyToMono(OddsDto.class)
                    .block();
            return Optional.of(OddsConverter.toModel(oddsDto));
        } catch (WebClientResponseException.NotFound e) {
            return Optional.empty();
        } catch (Exception e) {
            throw new ServiceUnavailableException();
        }
    }
}
