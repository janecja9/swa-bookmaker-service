package cz.cvut.fel.swa.bookmaker.bookmakerservice.client.dto;

import lombok.Data;

@Data
public class OddsDto {
    public Long id;
}
