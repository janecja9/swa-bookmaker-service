package cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto;

import lombok.Data;

@Data
public class OddsDto {
    public Long id;
}
