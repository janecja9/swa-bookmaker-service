package cz.cvut.fel.swa.bookmaker.bookmakerservice.api.converter;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto.BetDetailDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto.BetDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto.OddsDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto.UserDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Bet;

import java.util.Collection;

public class BetConverter {

    public static Bet toModel(BetDto betDto) {
        Bet bet = new Bet();
        bet.setId(betDto.getId());
        bet.setUserId(betDto.getUserId());
        bet.setOddsId(betDto.getOddsId());
        bet.setValue(betDto.getValue());
        return bet;
    }

    public static BetDetailDto fromModel(Bet bet) {
        UserDto userDto = new UserDto();
        userDto.setId(bet.getUserId());
        OddsDto oddsDto = new OddsDto();
        oddsDto.setId(bet.getOddsId());

        BetDetailDto betDetailDto = new BetDetailDto();
        betDetailDto.setId(bet.getId());
        betDetailDto.setUser(userDto);
        betDetailDto.setOdds(oddsDto);
        betDetailDto.setValue(bet.getValue());
        return betDetailDto;
    }

    public static Collection<BetDetailDto> fromModels(Collection<Bet> bets) {
        return bets.stream().map(BetConverter::fromModel).toList();
    }
}
