package cz.cvut.fel.swa.bookmaker.bookmakerservice.dao;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Bet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BetDaoRepository extends JpaRepository<Bet, Long> {

    List<Bet> findAllByUserId(Long userId);
}
