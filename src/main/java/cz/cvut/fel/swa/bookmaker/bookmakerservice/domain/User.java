package cz.cvut.fel.swa.bookmaker.bookmakerservice.domain;

import lombok.Data;

@Data
public class User {
    public Long id;
}
