package cz.cvut.fel.swa.bookmaker.bookmakerservice.client;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.converter.UserConverter;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.dto.UserDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.exception.ServiceUnavailableException;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserClient {

    private final LoadBalancerClient loadBalancerClient;
    private final WebClient.Builder webClientBuilder;

    public Optional<User> getUser(Long id) throws ServiceUnavailableException {
        ServiceInstance instance = loadBalancerClient.choose("USER-SERVICE");
        if (instance == null) {
            throw new ServiceUnavailableException();
        }

        String baseUrl = instance.getUri().toString();
        try {
            var userDto = webClientBuilder
                    .baseUrl(baseUrl)
                    .build()
                    .get()
                    .uri("/users/{id}", id)
                    .retrieve()
                    .bodyToMono(UserDto.class)
                    .block();
            return Optional.of(UserConverter.toModel(userDto));
        } catch (WebClientResponseException.NotFound e) {
            return Optional.empty();
        } catch (Exception e) {
            throw new ServiceUnavailableException();
        }
    }
}
