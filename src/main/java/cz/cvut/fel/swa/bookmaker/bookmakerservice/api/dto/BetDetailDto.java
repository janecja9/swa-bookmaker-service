package cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto;

import lombok.Data;

@Data
public class BetDetailDto {
    private Long id;
    private UserDto user;
    private OddsDto odds;
    private Integer value;
}
