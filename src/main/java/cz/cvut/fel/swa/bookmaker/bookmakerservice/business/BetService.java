package cz.cvut.fel.swa.bookmaker.bookmakerservice.business;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.dao.BetDaoRepository;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Bet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BetService {

    private final BetDaoRepository betDaoRepository;

    public void create(Bet entity) {
        betDaoRepository.save(entity);
    }

    public List<Bet> findAllByUserId(Long userId) {
        return betDaoRepository.findAllByUserId(userId);
    }
}
