package cz.cvut.fel.swa.bookmaker.bookmakerservice.api.dto;

import lombok.Data;

@Data
public class BetDto {
    private Long id;
    private Long userId;
    private Long oddsId;
    private Integer value;
}
