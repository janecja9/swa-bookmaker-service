package cz.cvut.fel.swa.bookmaker.bookmakerservice.client.converter;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.dto.OddsDto;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Odds;

public class OddsConverter {

    public static Odds toModel(OddsDto oddsDto) {
        Odds odds = new Odds();
        odds.setId(oddsDto.getId());
        return odds;
    }
}
