package cz.cvut.fel.swa.bookmaker.bookmakerservice.client.dto;

import lombok.Data;

@Data
public class UserDto {
    public Long id;
}
