package cz.cvut.fel.swa.bookmaker.bookmakerservice.api.controller;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.business.BetService;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.OddsClient;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.client.UserClient;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Odds;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BetController.class)
public class BetControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserClient userClient;

    @MockBean
    private OddsClient oddsClient;

    @MockBean
    private BetService betService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(new BetController(userClient, oddsClient, betService)).build();
    }

    @Test
    public void testGetAllBets_UserNotFound() throws Exception {
        Long userId = 1L;
        when(userClient.getUser(userId)).thenReturn(Optional.empty());

        mockMvc.perform(get("/users/{id}/bets", userId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllBets_UserFound() throws Exception {
        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        when(userClient.getUser(userId)).thenReturn(Optional.of(user));

        mockMvc.perform(get("/users/{id}/bets", userId))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateBet_UserOrOddsNotFound() throws Exception {
        Long userId = 1L;
        String betDtoJson = "{\"userId\":1,\"oddsId\":1,\"value\":100}";
        when(userClient.getUser(userId)).thenReturn(Optional.empty());

        mockMvc.perform(post("/users/{id}/bets", userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(betDtoJson))
                        .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateBet_Success() throws Exception {
        Long userId = 1L;
        String betDtoJson = "{\"userId\":1,\"oddsId\":1,\"value\":100}";
        User user = new User();
        user.setId(userId);
        Odds odds = new Odds();
        odds.setId(1L);

        when(userClient.getUser(userId)).thenReturn(Optional.of(user));
        when(oddsClient.getOdds(1L)).thenReturn(Optional.of(odds));

        mockMvc.perform(post("/users/{id}/bets", userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(betDtoJson))
                        .andExpect(status().isCreated());
    }
}