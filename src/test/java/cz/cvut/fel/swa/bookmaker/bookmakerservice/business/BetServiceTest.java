package cz.cvut.fel.swa.bookmaker.bookmakerservice.business;

import cz.cvut.fel.swa.bookmaker.bookmakerservice.dao.BetDaoRepository;
import cz.cvut.fel.swa.bookmaker.bookmakerservice.domain.Bet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BetServiceTest {

    @Mock
    private BetDaoRepository betDaoRepository;

    @InjectMocks
    private BetService betService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreate() {
        Bet bet = new Bet();
        betService.create(bet);
        verify(betDaoRepository, times(1)).save(bet);
    }

    @Test
    public void testFindAllByUserId() {
        Long userId = 1L;
        Bet bet1 = new Bet();
        bet1.setUserId(userId);
        Bet bet2 = new Bet();
        bet2.setUserId(userId);

        List<Bet> bets = Arrays.asList(bet1, bet2);
        when(betDaoRepository.findAllByUserId(userId)).thenReturn(bets);

        List<Bet> result = betService.findAllByUserId(userId);
        assertEquals(2, result.size());
        verify(betDaoRepository, times(1)).findAllByUserId(userId);
    }
}
